@php
    $texts = [
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, veniam esse. Sunt dolorem dolores accusantium nostrum, a optio reiciendis maiores necessitatibus rerum, inventore soluta, fuga tempora commodi magnam repellendus libero!',
        'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam, distinctio.'
    ];
    $projectTexts = [
        'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Accusantium!',
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam unde nisi cum sit nostrum labore?',
        'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Aliquid, dolores.',
        'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Beatae et quis labore quasi voluptatibus unde nostrum minima consequuntur possimus ullam.'
    ]
@endphp
@extends('layouts.main')
@section('content')
    <div class="d-flex flex-row card">
        <img src="https://bigenc.ru/media/2016/10/27/1235246285/29038.jpg" class="flex-shrink-0" style="max-width: 300px"/>
        <div class="card-body">
        <h5 class="card-title">Автор 1</h5>
        <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi dolor non cum dolorum molestiae amet maiores nobis veritatis, cupiditate nesciunt laboriosam reiciendis culpa soluta sapiente aperiam eius.</p>
        </div>
    </div>
    @foreach ($texts as $text)
        <figure class="mt-4 p-3 mb-2 bg-gradient text-dark">
            <div class="d-flex align-items-center">
            <span class="fas fa-quote-left p-1 flex-shrink-0 align-self-start"></span>
            <div class="flex-grow-1 ms-3">
                <blockquote class="blockquote">
                        <div class="flex-grow-1 ms-3">
                            <p>{{ $text }}</p>
                        </div>
                </blockquote>
                <figcaption class="blockquote-footer">
                    Книга 2 <cite title="Source Title">13.03.2022</cite>
                </figcaption>
            </div>
            <span class="fas fa-quote-right p-1 flex-shrink-0 align-self-end"></span>
        </figure>
    @endforeach
    <h2 class="display-5 mt-4">Работы автора</h2>
    <div class="row row-cols-1 row-cols-md-3 g-4 mt-2">
        @for ($i = 1; $i <= 4; $i++)
        <div class="col">
            <div class="card border border-dark shadow-0 mb-3">
                <div class="card-header bg-transparent border-dark">Работа {{ $i }}</div>
                <div class="card-body text-dark">
                    <p class="card-text">{{ $projectTexts[$i-1] }}</p>
                    <a href="/project/1">Посмотреть работу</a>
                    <p class="card-title text-muted text-end">13.03.2022</p>
                </div>
            </div>
        </div>
        @endfor
      </div>
@endsection