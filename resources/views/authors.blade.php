@extends('layouts.main')
@section('content')
@for ($i = 1; $i <= 3; $i++)
<div class="d-flex flex-row card mt-4">
    <div class="bg-image hover-overlay ripple flex-shrink-1" data-mdb-ripple-color="light">
      <img src="https://bigenc.ru/media/2016/10/27/1235246285/29038.jpg" class="img-fluid"/>
      <a href="/author/1">
        <div class="mask" style="background-color: rgba(251, 251, 251, 0.15);"></div>
      </a>
    </div>
    <div class="card-body w-100">
      <h5 class="card-title">Автор {{ $i }}</h5>
      <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quasi dolor non cum dolorum molestiae amet maiores nobis veritatis, cupiditate nesciunt laboriosam reiciendis culpa soluta sapiente aperiam eius odit earum incidunt?</p>
      <a href="/author/1" class="btn btn-outline-dark btn-rounded" data-mdb-ripple-color="dark">Посмотреть работы</a>
    </div>
</div>
@endfor
@endsection